﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class HomeController : Controller
    {
        UserDBLogic usdb = new UserDBLogic();
        ApartmentDBLogic apdb = new ApartmentDBLogic();
        CommentDBLogic cdb = new CommentDBLogic();

        private static object obj = new object();
        public static CustomModel cs = new CustomModel();

        public ActionResult Index()
        {
            int day = 1;
            Random rand = new Random();
            List<Amenity> listOfAmenities = new List<Amenity>();
            listOfAmenities.Clear();
            List<int> randomList = new List<int>();
            string date = "";
           
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    foreach (var ap in access.Apartments)
                    {
                        randomList.Clear();
                        ap.availabilityByDates = "";
                        for (int i = 0; i < 10; i++)
                        {
                            do
                            {
                                day = rand.Next(1, 30);
                            }
                            while (randomList.Contains(day));
                            randomList.Add(day);
                            date = day.ToString() + "/9/2019";
                            ap.availabilityByDates += date + "+";
                        }
                        Comment cm = new Comment();
                        cm.content = "Random komentar: oznaka:" + day.ToString();
                        cm.apartmentId = ap.id;
                        cm.approved = true;
                        cm.userId = 3;
                        //cdb.AddComment(cm);
                        apdb.EditApartment(ap);
                    }
                    foreach (var am in access.Amenities)
                    {
                        if (am.isDeleted == false)
                        {
                            listOfAmenities.Add(am);
                        }
                    }
                }
            }
            User user = (User)Session["User"];
            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }
            ViewBag.user = user;
            ViewBag.listofamenities = listOfAmenities;

            cs.Table1.Clear();
            cs.Table2.Clear();
            lock (obj)
            {
                using (var acess = new DatabaseAccess())
                {
                    var apartments = acess.Apartments;
                    foreach (var apartment in apartments)
                    {
                        if (apartment.isDeleted == false)
                        {
                            cs.Table1.Add(apartment);
                        }                       
                    }
                }
            }

            lock (obj)
            {
                using (var acess = new DatabaseAccess())
                {
                    var users = acess.Users;
                    foreach (var usera in users)
                    {
                        if(usera.isDeleted == false)
                        { 
                        cs.Table2.Add(usera);
                        }
                    }
                }
            }
            return View(cs);        
        }

     
        [HttpPost]
        // POST: Login
        public ActionResult SignUp(SignUpFormParams items)
        {
            User newUser = (User)Session["User"];
         
            if (newUser == null)
            {
                newUser = new User();
                Session["User"] = newUser;
            }
            if (newUser.loggedIn == true)
            {
                return RedirectToAction("Index");
            }
            newUser.isDeleted = false;
            newUser.lastname = items.surname;
            newUser.name = items.name;
            newUser.password = items.password;
            newUser.username = items.username;
            newUser.gender = items.gender;
            //inicijalna vrednost za svakog novoregistrovanog.
            newUser.role = "guest";
            if(usdb.AddUser(newUser)== false)
            {
                return View("Error");
            }
            newUser.loggedIn = true;
            ViewBag.user = newUser;
            return View("SignUp"); 
        }      
    }   
}