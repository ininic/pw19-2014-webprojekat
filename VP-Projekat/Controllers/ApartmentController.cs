﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class ApartmentController : Controller
    {

        ApartmentDBLogic adb = new ApartmentDBLogic();
        UserDBLogic udb = new UserDBLogic();
        private static object obj = new object();
        // GET: Apartment
        public ActionResult Index(int id, int b)
        {
            Apartment ap = new Apartment();
            List<Amenity> listOfAmenities = new List<Amenity>();
            listOfAmenities.Clear();
            lock (obj)
            {
                using (var acess = new DatabaseAccess())
                {
                    var amenities = acess.Amenities;
                    foreach (var amenity in amenities)
                    {
                        if (amenity.isDeleted == false)
                        {
                            listOfAmenities.Add(amenity);
                        }

                    }

                }
            }
            User user = (User)Session["User"];
            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }
            ViewBag.id = id;
            ViewBag.b = b;
            ViewBag.username = user.username;
            if (b == 1)
            {
                ap = adb.FindApartment(id);
                adb.DeleteApartmant(ap);
            }
            else
            {
                ap = adb.FindApartment(id);

            }
            CustomApartmentModel cam = new CustomApartmentModel();
            cam.ap = ap;
            cam.listOfAmenities = listOfAmenities;
            User us = new User();
            us = udb.FindUser(user.username);
            if (b == 3)
            {
                if (us.role == "host")
                {
                    return View(cam);
                }
                else
                {
                    return View("Error");
                }
            }
            else if (b == 2)
            {
                if (ap.host == user.username || user.role == "admin")
                {
                    return View(cam);
                }
                else
                {
                    return View("Error");
                }
            }
            else if (b == 5 || b == 1 || b == 4)
            {
                return View(cam);
            }
            else
            {
                return View("Error");
            }

        }

        [HttpPost]
        public ActionResult Edit(ApartmentParams items)
        {
            Apartment ap = new Apartment();
            ap = adb.FindApartment(items.id);
            ap.host = items.host;
            ap.numberOfGuests = items.numberOfGuests;
            ap.numberOfRooms = items.numberOfRooms;
            ap.pricePerNight = items.pricePerNight;
            ap.location = items.drzava;
            ap.location += ", " + items.grad;
            ap.location += ", " + items.ulica;
            ap.location += "  br. " + items.broj;
            ap.type = items.type;
            if (items.listofamenities == null)
            {
                ap.listOfAmenities = "";
            }
            else
            {
                for (int i = 0; i < items.listofamenities.Length; i++)
                {
                    ap.listOfAmenities += items.listofamenities[i];
                }
            }
            adb.EditApartment(ap);
            return View("Result");

        }
        [HttpPost]
        public ActionResult Add(ApartmentParams items)
        {
            Apartment ap = new Apartment();
            ap.host = items.host;
            ap.numberOfGuests = items.numberOfGuests;
            ap.numberOfRooms = items.numberOfRooms;
            ap.pricePerNight = items.pricePerNight;
            ap.location = items.drzava;
            ap.location += ", " + items.grad;
            ap.location += ", " + items.ulica;
            ap.location += "  br. " + items.broj;
            ap.type = items.type;
            ap.status = "aktivan";
            if (items.listofamenities == null)
            {
                ap.listOfAmenities = "";
            } 
            else
            {   
                for(int i=0; i<items.listofamenities.Length; i++)
                {
                    ap.listOfAmenities += items.listofamenities[i];
                }
            }
            adb.AddApartment(ap);
            return View("Result");

        }
    }
}