﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    
    public class CommentController : Controller
    {
        // GET: Comment
        UserDBLogic usdb = new UserDBLogic();
        private static object obj = new object();
        List<Comment> listOfComments = new List<Comment>();
        List<User> listOfUsers = new List<User>();
        public ActionResult Index(int id)
        {
            listOfComments.Clear();
            listOfUsers.Clear();
            lock (obj)
            {
                using (var acess = new DatabaseAccess())
                {
                   
                    var comments = acess.Comments;
                    foreach (var comment in comments)
                    {

                        //acess.Comments.Attach(comment);
                        //acess.Comments.Remove(comment);

                        User us = new User();
                        us = usdb.FindUserById(comment.userId);
                        if(us != null)
                        {
                            listOfUsers.Add(us);
                            listOfComments.Add(comment);
                        }                      
                    }
                    //acess.SaveChanges();
                }
                ViewBag.id = id;
                ViewBag.listOfUsers = listOfUsers;
            }
            return View(listOfComments);
        }
    }
}