﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class ProfileController : Controller
    {
        UserDBLogic udblog = new UserDBLogic();
        ApartmentDBLogic apdblog = new ApartmentDBLogic();
        // GET: Profile
        private static object obj = new object();
        public ActionResult Index()
        {
            User user = (User)Session["User"];
            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }
            ViewBag.user = user;
            User newUser = new Models.User();
            newUser = udblog.FindUser(user.username);

            return View(newUser);
        }
        [HttpPost]
        public ActionResult Profilee(ProfileParams items)
        {
            User user = (User)Session["User"];

            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }

            user.name = items.name;
            user.gender = items.gender;
            user.lastname = items.surname;
            user.password = items.password;
            User nwuser = new User();
            nwuser = udblog.FindUser(user.username);
            user.username = items.username;
            if (nwuser == null)
            {
                return View("Error");
            }
            else
            {
                nwuser.name = items.name;
                nwuser.lastname = items.surname;
                nwuser.gender = items.gender;
                nwuser.username = items.username;
                nwuser.password = items.password;
                udblog.EditUser(nwuser);
            }

            ViewBag.user = user;
            return View("Index", nwuser);
        }

        public ActionResult Promote(int id, int b)
        {
            User us = new Models.User();
            us = udblog.FindUserById(id);
            if (b == 1)
            {
                us.role = "host";
            }
            else
            {
                us.role = "guest";
            }
            udblog.EditUser(us);
            ViewBag.user = us;
            ViewBag.b = b;
            return View("Promote");
        }

        public ActionResult Details(int id, int b)
        {
            User us = new Models.User();
            us = udblog.FindUserById(id);
            Apartment ap = new Apartment();
            List<Apartment> lista = new List<Apartment>();
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var apartments = access.Apartments;

                    foreach (var apartment in apartments)
                    {
                        if (apartment.host == us.username)
                        {
                            lista.Add(apartment);
                        }
                    }
                    ViewBag.user = us;
                    ViewBag.b = b;
                    return View("Details", lista);
                }

            }
        }
    }

}