﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class AmenityController : Controller
    {
        // GET: Amenity
        AmenityDBLogic adb = new AmenityDBLogic();
        UserDBLogic udb = new UserDBLogic();
        // GET: Amenity
        private static object obj = new object();
        List<Amenity> listOfAmenities = new List<Amenity>();

        public ActionResult Index()
        {
            listOfAmenities.Clear();
            lock (obj)
            {
                using (var acess = new DatabaseAccess())
                {
                    var amenities = acess.Amenities;
                    foreach (var amenity in amenities)
                    {
                        if(amenity.isDeleted == false)
                        { 
                            listOfAmenities.Add(amenity);
                        }

                    }

                }
            }
            User user = (User)Session["User"];
            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }
            User us = new User();
            us = udb.FindUser(user.username);
            //if (b == 3)
            //{
                if (us.role == "admin")
                {
                return View(listOfAmenities);
                }
                else
                {
                    return View("Error");
                }
                
        }

        [HttpPost]
        // POST: Login
        public ActionResult Add(Amenity items)
        {
            Amenity am = new Amenity();
            am.name = items.name;
            // am = adb.FindAmenity(items.id);
            adb.AddAmenity(am);
            listOfAmenities.Add(am);
            return View("Result", am);
        }
        public ActionResult Delete(int id, int b)
        {
            Amenity am = new Amenity();
            am = adb.FindAmenity(id);
            adb.DeleteAmenity(am);
            listOfAmenities.Remove(am);
            ViewBag.b = b;
            return View("Result", am);
        }
    }
}