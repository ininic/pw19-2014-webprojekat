﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ApartmentDBLogic adb = new ApartmentDBLogic();
        public ActionResult Index(int id, int b)
        {
            List<UploadedFile> list = new List<UploadedFile>();
            ViewBag.uploadedFiles = list;
            Apartment ap = new Apartment();
            ap = adb.FindApartment(id);
            ViewBag.id = id;
            ViewBag.apartment = ap;

            if (b == 4)
            {
                return View("Images", id);
            }
            return View(id);
        }

        [HttpPost]
        public ActionResult Upload(int imgid)
        {
            Random rand = new Random();
            List<UploadedFile> list = new List<UploadedFile>();
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    if (file != null && file.ContentLength > 0)
                    {
                        // var fileName = Path.GetFileName(file.FileName);
                        //trebalo bi generisati jedinstvene slucajne brojeve, jer u suporotnom ako se pogode
                        // dva ista naziva slika, slika nece biti dodata, mada su za to jako male sanse
                        //tako da ostaje ovo jednostavnije resenje
                        var fileName = rand.Next().ToString() + ".png";
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                        file.SaveAs(path);
                        UploadedFile uf = new UploadedFile(fileName, path);
                        list.Add(uf);
                        Apartment ap = new Apartment();
                        ap = adb.FindApartment(imgid);
                        ap.images += "/" + fileName;
                        ViewBag.apartment = ap;
                        adb.EditApartment(ap);
                        ViewBag.id = ap.id;
                    }
                }
            }
            ViewBag.uploadedFiles = list;
           
            return View("Index");
        }
    }
}