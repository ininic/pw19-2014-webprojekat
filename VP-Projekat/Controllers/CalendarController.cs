﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class CalendarController : Controller
    {
        ApartmentDBLogic adb = new ApartmentDBLogic(); 
        // GET: Calendar
        public ActionResult Index(int id, int b)
        {
            Apartment ap = new Apartment();
            ap = adb.FindApartment(id);


            return View(ap);
        }
    }
}