﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Controllers
{
    public class LoginController : Controller
    {
        UserDBLogic udblog = new UserDBLogic();
        // GET: Login
        public ActionResult Index()
        {
            User user = (User)Session["User"];
            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }

            ViewBag.user = user;
            return View();
        }

        [HttpPost]
        // POST: Login
        public ActionResult Login(LoginFormParams items)
        {            
            User user = (User)Session["User"];           

            if (user == null)
            {
                user = new User();
                Session["User"] = user;
            }

            if (user.loggedIn == true)
            {
                return RedirectToAction("Index");
            }
            User nwuser = new User();
            nwuser = udblog.FindUser(items.username);

            if (nwuser == null)
            {
                return View("Error");
            }
            else
            {
                if (items.password.Equals(nwuser.password))
                {
                    user.role = nwuser.role;
                    user.username = items.username;
                    user.password = items.password;
                    user.loggedIn = true;
                }
                else
                {
                    return View("Error");
                }
            }

            ViewBag.user = user;

            return View("Result"); 
        }

        // GET: SignOff
        public ActionResult LogOut()
        {
            Session.Abandon();
            User user = new User();
            Session["User"] = user;
            user.loggedIn = false;
            ViewBag.user = user;
            return View("LogOut");
        }
    }
}