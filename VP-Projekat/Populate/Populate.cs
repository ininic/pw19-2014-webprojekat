﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VP_Projekat.DatabaseLogic;
using VP_Projekat.Models;

namespace VP_Projekat.Populate
{
    public class Populate
    {
        public UserDBLogic usdblog = new UserDBLogic();
        public ApartmentDBLogic apdblog = new ApartmentDBLogic();
        public AmenityDBLogic amdblog = new AmenityDBLogic();
        public CommentDBLogic cmdblog = new CommentDBLogic();

        public void populateFunction()
        {
            User u1 = new User("ininic", "123", "Ivan", "Ninic", "male", "admin", "", "","", false, false);
            User u2 = new User("ppetrovic", "123", "Petar", "Petrovic", "male", "admin", "", "", "", false, false);
            User u3 = new User("niko", "123", "Nikola", "Nikolic", "male", "admin", "", "", "", false, false);
            User u4 = new User("mare", "123", "Marko", "Markovic", "male", "admin", "", "", "", false, false);
            User u5 = new User("milana", "123", "Milana", "Milanovic", "male", "admin", "", "", "", false, false);
            User u6 = new User("joca", "123", "Jovana", "Jovanovic", "male", "admin", "", "", "", false, false);
            User u7 = new User("alex", "123", "Aleksandra", "Aleksandrovic", "male", "admin", "", "", "", false, false);
        }
     
    }
}