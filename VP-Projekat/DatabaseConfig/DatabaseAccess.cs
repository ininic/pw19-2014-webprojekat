﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VP_Projekat.Models;

namespace VP_Projekat.DatabaseConfig
{
    public class DatabaseAccess : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<Amenity> Amenities { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DatabaseAccess() : base("DataBaseConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseAccess, Configuration>());
        }

    }
}