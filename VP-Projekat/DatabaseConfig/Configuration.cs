﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Migrations;

namespace VP_Projekat.DatabaseConfig
{
    class Configuration : DbMigrationsConfiguration<DatabaseAccess>
    {
        public Configuration()
        {
            //Configurating Database
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Podaci";
        }
    }
}