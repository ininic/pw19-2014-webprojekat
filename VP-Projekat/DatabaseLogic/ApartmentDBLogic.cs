﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.Models;

namespace VP_Projekat.DatabaseLogic
{
    public class ApartmentDBLogic
    {

        private static object obj = new object();
        public Apartment FindApartment(string id)
        {
            Apartment u = new Apartment();
            bool boolvalue = false;
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var apartmets = access.Apartments;

                    foreach (var apartmet in apartmets)
                    {
                        if (apartmet.host == id)
                        {
                            boolvalue = true;
                            u = apartmet;
                        }
                    }
                }
                if (boolvalue)
                {
                    Console.WriteLine("Apartman " + id + " je pronadjen!");
                    return u;
                }
                else
                {
                    Console.WriteLine("Apartman " + id + " nije pronadjen!");
                }

                return null;
            }
        }

        public bool DeleteApartmant(Apartment n)
        {
            using (var access = new DatabaseAccess())
            {

                int valid = 0;
                //access.Apartments.Attach(n);
                // access.Apartments.Remove(n);
                foreach (var ap in access.Apartments)
                {
                    if (ap.id == n.id)
                    {
                        if (ap.isDeleted == false)
                        {
                            ap.isDeleted = true;
                            goto label;
                        }
                    }
                }


                    label:
                    {
                    }
                    try
                    {
                        valid = access.SaveChanges();

                        if (valid > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
        

        public Apartment FindApartment(int id)
        {
            Apartment a = new Apartment();
            bool boolvalue = false;
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var apartments = access.Apartments;

                    foreach (var apartment in apartments)
                    {
                        if (apartment.id == id)
                        {
                            boolvalue = true;
                            a = apartment;
                        }
                    }
                }
                if (boolvalue)
                {
                    Console.WriteLine("Apartman " + id + " je pronadjen!");
                    return a;
                }
                else
                {
                    Console.WriteLine("Apartman " + id + " nije pronadjen!");
                }

                return null;
            }
        }

        public bool EditApartment(Apartment n)
        {
            using (var access = new DatabaseAccess())
            {
                int valid = 0;
                foreach (var nt in access.Apartments)
                {
                    if (nt.id == n.id)
                    {
                        nt.host = n.host;
                        nt.location = n.location;
                        nt.pricePerNight = n.pricePerNight;
                        nt.numberOfGuests = n.numberOfGuests;
                        nt.numberOfRooms = n.numberOfRooms;
                        nt.type = n.type;
                        nt.images = n.images;
                        nt.listOfAmenities = n.listOfAmenities;
                        nt.availabilityByDates = n.availabilityByDates;

                        goto label;
                    }
                }

                label:
                {
                }
                try
                {
                    valid = access.SaveChanges();

                    if (valid > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    AddApartment(n);
                    return true;
                }
            }
        }

        public bool AddApartment(Apartment newNote)
        {
            using (var access = new DatabaseAccess())
            {
                newNote.isDeleted = false;
                access.Apartments.Add(newNote);
                int valid = access.SaveChanges();

                if (valid > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}