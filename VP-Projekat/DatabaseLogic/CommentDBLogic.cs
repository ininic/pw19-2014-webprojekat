﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.Models;

namespace VP_Projekat.DatabaseLogic
{
    public class CommentDBLogic
    {
        private static object obj = new object();
        public bool AddComment(Comment newComment)
        {
            using (var access = new DatabaseAccess())
            {
                access.Comments.Add(newComment);
                int valid = access.SaveChanges();

                if (valid > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}