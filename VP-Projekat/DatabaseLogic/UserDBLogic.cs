﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.Models;

namespace VP_Projekat.DatabaseLogic
{
    public class UserDBLogic
    {
        private static object obj = new object();

        public bool AddUser(User newUser)
        {
            lock (obj)
            {
                using (var acess = new DatabaseAccess())
                {
                    var users = acess.Users;
                    foreach (var user in users)
                    {
                        if (user.username == newUser.username)
                        {
                            if(user.isDeleted == false)
                            {
                                //postoji u bazi i nije logicki izbrisan
                            return false;
                            }
                            else
                            {
                                //postoji u bazi i logički je izbrisan, tako da možemo dodati novi
                                //sa istim nazivom
                                acess.Users.Add(newUser);
                            }
                        }
                    }

                    acess.Users.Add(newUser);
                    int valid = acess.SaveChanges();
                    if (valid > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public bool EditUser(User u)
        {
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    int valid = 0;
                    foreach (var nt in access.Users)
                    {
                        if (nt.id == u.id)
                        {
                            nt.username = u.username;
                            nt.password = u.password;
                            nt.name = u.name;
                            nt.lastname = u.lastname;
                            nt.gender = u.gender;
                            nt.role = u.role;
                            goto label;
                        }
                    }
                    label: { }
                    try
                    {
                        valid = access.SaveChanges();

                        if (valid > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch
                    {
                        AddUser(u);
                        return true;
                    }
                }
            }
        }

        public User FindUser(string username)
        {
            User u = new User();
            bool boolvalue = false;
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var users = access.Users;

                    foreach (var user in users)
                    {
                        if (user.username == username)
                        {
                            if(user.isDeleted == false)
                            {
                            boolvalue = true;
                            u = user;
                            }
                        }
                    }
                }
                if (boolvalue)
                {
                    Console.WriteLine("Korisnik " + username + " je pronadjen!");
                    return u;
                }
                else
                {
                    Console.WriteLine("Korisnik " + username + " nije pronadjen!");
                }

                return null;
            }
        }

        public User FindUserById(int id)
        {
            User u = new User();
            bool boolvalue = false;
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var users = access.Users;

                    foreach (var user in users)
                    {
                        if (user.id == id)
                        {
                            boolvalue = true;
                            u = user;
                        }
                    }
                }
                if (boolvalue)
                {
                    Console.WriteLine("Korisnik " + id + " je pronadjen!");
                    return u;
                }
                else
                {
                    Console.WriteLine("Korisnik " + id + " nije pronadjen!");
                }

                return null;
            }
        }

        public User UserLogin(string username, string password)
        {
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var users = access.Users;

                    foreach (var user in users)
                    {
                        if (user.username == username && user.password == password)
                        {
                            return user;
                        }
                    }
                }
                return null;
            }
        }
    }
}