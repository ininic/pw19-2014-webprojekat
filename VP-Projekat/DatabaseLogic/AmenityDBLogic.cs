﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VP_Projekat.DatabaseConfig;
using VP_Projekat.Models;

namespace VP_Projekat.DatabaseLogic
{
    public class AmenityDBLogic
    {

        private static object obj = new object();

        public bool DeleteAmenity(Amenity a)
        {
            using (var access = new DatabaseAccess())
            {
                int valid = 0;
                //access.Apartments.Attach(n);
                // access.Apartments.Remove(n);
                foreach (var am in access.Amenities)
                {
                    if (am.id == a.id)
                    {
                        if (am.isDeleted == false)
                        {

                            am.isDeleted = true;
                            goto label;
                        }
                    }
                }


                label:
                {
                }
                try
                {
                    valid = access.SaveChanges();

                    if (valid > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {                  
                    return false;
                }
            }
        }

        public bool AddAmenity(Amenity a)
        {
            using (var access = new DatabaseAccess())
            {

                access.Amenities.Add(a);
                int valid = access.SaveChanges();

                if (valid > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public Amenity FindAmenity(int id)
        {
            Amenity a = new Amenity();
            bool boolvalue = false;
            lock (obj)
            {
                using (var access = new DatabaseAccess())
                {
                    var amenities = access.Amenities;

                    foreach (var amenity in amenities)
                    {
                        if (amenity.id == id)
                        {
                            boolvalue = true;
                            a = amenity;

                        }
                    }
                }
                if (boolvalue)
                {
                    Console.WriteLine("Korisnik " + id + " je pronadjen!");
                    return a;
                }
                else
                {
                    Console.WriteLine("Korisnik " + id + " nije pronadjen!");
                }

                return null;
            }

        }
    }
}
