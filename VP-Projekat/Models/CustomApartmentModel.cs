﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class CustomApartmentModel
    {

        public List<Amenity> listOfAmenities { get; set; }

       public Apartment ap { get; set; }

        public CustomApartmentModel()
        {
            this.listOfAmenities = new List<Amenity>();
            this.ap = new Apartment();
        }
    }
}