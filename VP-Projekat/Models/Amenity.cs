﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class Amenity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string name { get; set; }
        public bool isDeleted { get; set; }


        public Amenity() { }

        public Amenity(string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}