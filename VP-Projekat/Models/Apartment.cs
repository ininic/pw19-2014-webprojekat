﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class Apartment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string type { get; set; }
        public int numberOfRooms { get; set; }
        public int numberOfGuests { get; set; }
        public string location { get; set; }
        public float pricePerNight { get; set; }
        public string host { get; set; }
        public string status { get; set; }
        public string listOfAmenities { get; set; }
        public string images { get; set; }
        public string availabilityByDates { get; set; }
        public bool isDeleted { get; set; }

        public Apartment() { }

        public Apartment(string username, string password)
        {
            this.id = id;
            this.type = type;
        }

        public Apartment(string type, int numberOfRooms, int numberOfGuests, string host, float pricePerNight, string location, string listOfAmenities, string status, string availabilityByDates, bool isDeleted)
        {
            this.id = id;
            this.type = type;
            this.numberOfRooms = numberOfRooms;
            this.numberOfGuests = numberOfGuests;
            this.host = host;
            this.pricePerNight = pricePerNight;
            this.location = location;
            this.status = status;
            this.listOfAmenities = listOfAmenities;
            this.availabilityByDates = availabilityByDates;
            this.isDeleted = isDeleted;
        }
    }
}