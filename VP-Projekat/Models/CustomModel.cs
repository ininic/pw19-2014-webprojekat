﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class CustomModel
    {
        public List<Apartment> Table1 { get; set; }

        public List<User> Table2 { get; set; }

        public CustomModel()
        {
            this.Table1 = new List<Apartment>();
            this.Table2 = new List<User>();
        }
    }
}