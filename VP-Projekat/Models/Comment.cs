﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int userId { get; set; }
        public int apartmentId { get; set; }
        public string content { get; set; }
        public bool isDeleted { get; set; }

        public bool approved { get; set; }
        public Comment() { }

        public Comment(int userId, int apartmentId, string content, bool approved)
        {
            this.userId = userId;
            this.apartmentId = apartmentId;
            this.content = content;
            this.approved = approved;
        }
    }
}