﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class ProfileParams
    {
        public string name
        {
            get;
            set;
        }

        public string surname
        {
            get;
            set;
        }

        public string gender
        {
            get;
            set;
        } 
        public string username
        {
            get;
            set;
        }
        public string password
        {
            get;
            set;
        }
    }
}