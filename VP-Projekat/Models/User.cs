﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string gender { get; set; }
        public string role { get; set; }
        public string apartmentsForRent { get; set; }
        public string rentedApartments { get; set; }
        public string listOfReservation { get; set; }
        public bool loggedIn { get; set; }
        public bool isDeleted { get; set; }

        public User() { }

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public User(string username, string password, string name, string lastname, string gender, string role, string apartmentsForRent, string rentedApartments, string listOfReservation, bool loggedIn, bool isDeleted)
        {
            this.id = id;
            this.username = username;
            this.username = password;
            this.name = name;
            this.lastname = lastname;
            this.gender = gender;
            this.role = role;
            this.apartmentsForRent = apartmentsForRent;
            this.rentedApartments = rentedApartments;
            this.listOfReservation = listOfReservation;
            this.loggedIn = loggedIn;
            this.isDeleted = isDeleted;
        }
    }
}