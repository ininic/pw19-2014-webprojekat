﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class LoginFormParams
    {
        public string username
        {
            get;
            set;
        }

        public string password
        {
            get;
            set;
        }

    }
}