﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class ApartmentParams
    {
        public string host
        {
            get;
            set;
        }

        public string drzava
        {
            get;
            set;
        }
        public string grad
        {
            get;
            set;
        }
        public string ulica
        {
            get;
            set;
        }
        public string broj
        {
            get;
            set;
        }
        public int numberOfRooms
        {
            get;
            set;
        }
        public int pricePerNight
        {
            get;
            set;
        }
        public int numberOfGuests
        {
            get;
            set;
        }
        public string type
        {
            get;
            set;
        }
        public int id
        {
            get;
            set;
        }
        public string[] listofamenities
        {
            get;
            set;
        }

    }
}