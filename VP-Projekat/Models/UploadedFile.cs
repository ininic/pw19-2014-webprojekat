﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VP_Projekat.Models
{
    public class UploadedFile
    {

        public string name { get; set; }
        public string localPath { get; set; }

        public UploadedFile(string name, string localPath)
        {
            this.name = name;
            this.localPath = localPath;
        }
    }
}